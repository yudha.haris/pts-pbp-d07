from django.http.response import HttpResponse
from django.shortcuts import render
from .models import Lyrics
from .forms import LyricsForm
from django.http import HttpResponseRedirect, HttpResponse
from django.core import serializers

# generate output page
def outputLyrics(request):
    lyrics = Lyrics.objects.all().values()
    response = {"lyrics": lyrics}
    return render(request, "lyrics_out_template.html", response)


# generate input form page
def inputLyrics(request):
    # request by POST
    form = LyricsForm(request.POST)

    if form.is_valid():
        # save input to database
        form.save()
        # redirect to output page
        return HttpResponseRedirect("/wash-your-lyrics/output")

    response = {"form": form}

    return render(request, "lyrics_in_template.html", response)


# JSON response
def jsonLyrics(request):
    data = serializers.serialize("json", Lyrics.objects.all())
    return HttpResponse(data, content_type="application/json")