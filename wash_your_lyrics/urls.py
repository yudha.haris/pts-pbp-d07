from django.urls import path
from .views import inputLyrics, outputLyrics, jsonLyrics

urlpatterns = [
    path("input", inputLyrics, name="inputLyrics"),
    path("output", outputLyrics, name="outputLyrics"),
    path("json", jsonLyrics, name="jsonLyrics"),
]