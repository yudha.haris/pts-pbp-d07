const hex = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 'A', 'B', 'C', 'D', 'E', 'F'];

const surpriseBtn = document.getElementById("warna-btn");
surpriseBtn.addEventListener("click", setGeneratedColor);
function setGeneratedColor() {
    // set color hex code
    let listOfGeneratedColor = generateColorHexCode();

    // tackle edge case where colors aren't gradient
    while (listOfGeneratedColor[0] === listOfGeneratedColor[1]) {
        listOfGeneratedColor = generateColorHexCode();
    }

    // set color display
    let card = document.getElementsByClassName("row");
    for (let i = 0; i < card.length; i++) {
        card[i].style.backgroundImage = "linear-gradient(to right," + listOfGeneratedColor[0] + "," + listOfGeneratedColor[1] + ")";
    }
    let wallOfFame = document.getElementsByClassName("subtitle")[0];
    wallOfFame.style.color = listOfGeneratedColor[0];
}

function generateColorHexCode() {
    let colorOneGenerated = "#";
    let colorTwoGenerated = "#";
    for (let digit = 0; digit < 6; digit++) {
        colorOneGenerated += hex[generateNumber()];
        colorTwoGenerated += hex[generateNumber()];
    }
    return [colorOneGenerated, colorTwoGenerated];
}

function generateNumber() {
    return Math.floor(Math.random() * hex.length);
}