from django.contrib import admin
from .models import Lyrics

# Register Lyrics models
admin.site.register(Lyrics)