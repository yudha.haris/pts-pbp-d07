from django.http.response import HttpResponse, HttpResponseRedirect, JsonResponse
from django.shortcuts import render
from .models import Story
from .form import StoryForm
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from register.models import Role 

# Create your views here.
def story(request):
    stories = Story.objects.all()
    return render(request, "story.html", context={
        'stories' : stories
    })

@login_required(login_url='/register/login')
def add_story(request):
    if (Role.roles == "Belum Pernah"):
        messages.info(request, "You are not allowed to share story")
    else:
        if (request.method == "POST"):
            messages.info(request, f"{Role.roles}")
            form = StoryForm(request.POST or None)
            if (form.is_valid()):
                form.save()
                return HttpResponseRedirect('/story')
            else:
                return HttpResponse('Form not valid')
        else:
            form = StoryForm()
            return render(request, "write_stories.html", context={
                'form' : form
            })
