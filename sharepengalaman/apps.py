from django.apps import AppConfig


class SharepengalamanConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'sharepengalaman'
