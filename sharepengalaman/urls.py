from django.urls import path
from django.urls.conf import include
from .views import story, add_story

urlpatterns = [
    path('', story),
    path('write', add_story)
]