from django.db import models
from django.db.models.fields import CharField
from django.template.defaultfilters import slugify 
from django.urls import reverse

class Story(models.Model):
        title = models.CharField(max_length=50)
        by = models.CharField(max_length=50)
        experience = models.TextField()
        slug = models.SlugField(unique=True, null=True)
        def save(self):
                self.slug = slugify(self.title)
                self.title = self.title
                self.by = self.by
                self.experience = self.experience
                return super().save()