from django.shortcuts import render
from .forms import CommentForm
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from sharepengalaman.models import Story

@csrf_exempt
def detail(request, slug):
    story = Story.objects.get(slug=slug)
    commentz = story.comments.all()

    if request.method == 'POST':
        form = CommentForm(request.POST)

        if form.is_valid():
            obj = form.save(commit=False)
            obj.story = story
            obj.save()

            response_data = {}
            response_data['oleh'] = request.POST.get('oleh')
            response_data['body'] = request.POST.get('body')
            return JsonResponse(response_data)


    else:
        form = CommentForm()

    context = {
        'story': story,
        'form': form,
        'commentz': commentz,
    }

    return render(request, 'detail.html', context)
