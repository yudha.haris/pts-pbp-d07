from django.urls import path
from django.urls.conf import include
from .views import profile, edit


urlpatterns = [
    path('', profile, name='profile'),
    path('edit', edit, name='edit-profile')
]