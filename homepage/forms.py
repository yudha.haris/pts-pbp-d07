from django import forms
from .models import Feedback


class FeedbackForm(forms.ModelForm):

    class Meta:
        model = Feedback
        fields = "__all__"

        widgets = {
            'feedback': forms.Textarea(attrs={
                'class': 'form-control',
                'id': 'post-text',
                'required': True,
                'placeholder': 'Ketik di sini buat ngasih feedback :D'
            })
        }
