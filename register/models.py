from django.db import models

# Create your models here.
class Role(models.Model):
    ROLES = (
        ( 'P', 'Pernah'),
        ( 'BP', 'Belum Pernah'),
    )
    roles = models.CharField(max_length=50, choices=ROLES, blank=True, null=True)