from django.db import models

class Obat(models.Model):
    name=models.CharField(max_length=36)
    info=models.TextField()
    image=models.ImageField(default='defaultObat.jpg',upload_to='obat')
# Create your models here.
