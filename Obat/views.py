from django.shortcuts import render
from .forms import ObatForm
from .models import Obat
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth.decorators import login_required


def add_obat(request):
    if request.method == 'POST':
        form=ObatForm(request.POST,request.FILES)
        if form.is_valid():
            form.save()
            
        
    else:
        form=ObatForm()
    return render(request,'obat_form.html',{'form':form})

def list_obat(request):
    obats=Obat.objects.all()
    response={"obats":obats}
    return render(request,'obat_list.html',response)

# Create your views here.
